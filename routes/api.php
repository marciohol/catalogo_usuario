<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('users/{userId}/posts', 'PostController@show');

Route::apiResource('users', 'UserController');

Route::apiResource('imports', 'ImportController')->only([
    'store'
]);
