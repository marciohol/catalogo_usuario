<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;

use App\Models\User;

class PostController extends BaseController
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($userId)
    {
        $user = User::where('id', $userId)->with('posts')->first();

        if(!$user) {
            return $this->sendError('Usuário não encontrado');
        }

        return $this->sendResponse($user->toArray(), 'Usuário encontrado com sucesso');
    }
}
