<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Models\User;
use App\Models\Post;

use App\Services\ImportService;


class ImportController extends BaseController
{
    protected $importService;

    public function __construct(ImportService $importService)
    {
        $this->importService = $importService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user, Post $post)
    {
        try {
            $table = $request->input('table');
            $data = $request->input('data');

            if($table === 'users') {
                $this->importService->importUsers($data);
            }

            if($table === 'posts') {
                $this->importService->importPosts($data);
            }

            return $this->sendResponse([], 'Dados importados com sucesso.');
        } catch(\Exception $e) {
            return $this->sendError('Erro ao importar os dados.', [], 200);
        }
    }
}
