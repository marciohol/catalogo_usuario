<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Models\User;
use Validator;


class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return $this->sendResponse($users->toArray(), 'Usuários retornados com sucesso.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $messages = [
            'required' => 'O :attribute é obrigatório.',
        ];

        $validator = Validator::make($input, [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required'
        ], $messages);

        if($validator->fails()) {
            return $this->sendError('Erro de validação.', $validator->errors(), 400);
        }

        try {
            $user = User::create($input);
            return $this->sendResponse($user->toArray(), 'Usuário criado com sucesso.');
        } catch(\Exception $e) {
            return $this->sendError('Erro ao cadastrar usuário.', [], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if(!$user) {
            return $this->sendError('Usuário não encontrado');
        }

        return $this->sendResponse($user->toArray(), 'Usuário encontrado com sucesso');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $messages = [
            'required' => 'O :attribute é obrigatório.',
        ];

        $validator = Validator::make($input, [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required'
        ], $messages);

        if($validator->fails()) {
            return $this->sendError('Erro de validação.', $validator->errors(), 400);
        }

        try {
            $user = User::find($id);

            if(!$user) {
                return $this->sendError('Usuário não encontrado');
            }

            $user->update($input);

            return $this->sendResponse($user->toArray(), 'Usuário Atualizado com sucesso');
        } catch(\Exception $e) {
            return $this->sendError('Erro ao atualizar usuário.', [], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::find($id);

            if(!$user) {
                return $this->sendError('Usuário não encontrado');
            }

            $user->delete();

            return $this->sendResponse([], 'Usuário deletado com sucesso.');
        } catch(\Exception $e) {
            return $this->sendError('Erro ao deletar usuário.', [], 200);
        }
    }
}
