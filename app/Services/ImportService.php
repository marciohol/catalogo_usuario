<?php

namespace App\Services;

use App\Models\User;
use App\Models\Post;

class ImportService
{
    public function importUsers($data)
    {
        foreach($data as $item) {
            User::updateOrCreate([
                'id' => $item['id'],
                'name' => $item['name'],
                'phone' => $item['phone'],
                'username' => $item['username'],
                'website' => $item['website'],
                'email' => $item['email'],
                'street' => $item['address']['street'],
                'suite' => $item['address']['suite'],
                'city' => $item['address']['city'],
                'zipcode' => $item['address']['zipcode'],
                'lat' => $item['address']['geo']['lat'],
                'lng' => $item['address']['geo']['lng'],
                'companyName' => $item['company']['name'],
                'catchPhrase' => $item['company']['catchPhrase'],
                'bs' => $item['company']['bs'],
            ]);
        }
    }

    public function ImportPosts($data)
    {
        foreach($data as $item) {
            Post::updateOrCreate([
                'id' => $item['id'],
                'title' => $item['title'],
                'body' => $item['body'],
                'user_id' => $item['userId'],
            ]);
        }
    }
}

