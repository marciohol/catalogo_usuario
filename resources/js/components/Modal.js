import React from 'react';
import { Link } from 'react-router-dom'

export default function Modal(props) {
    const backdropStyle = {
        position: 'fixed',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'rgba(0,0,0,0.3)',
        padding: 50,
    };

    const modalStyle = {
        backgroundColor: '#fff',
        borderRadius: 5,
        maxWidth: 600,
        maxHeight: 330,
        padding: 0,
        position: 'absolute',
        marginTop: 'auto',
        marginBottom: 'auto',
        marginLeft: 'auto',
        marginRight: 'auto',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,

        display: 'block',
    };

    if(!props.show){
        return null;
    }

    return (
        <div className="backdrop" style={backdropStyle}>
            <div className="modal" style={modalStyle}>
                <div className="card">
                    <h5 className="card-header">#{props.data.name}</h5>
                    <div className="card-body">
                        <h5 className="card-title">{props.data.email}</h5>
                        <p className="card-text">
                            <strong>Usuário:</strong> {props.data.username} <br/>
                            <strong>Endereço:</strong> {props.data.street} {props.data.suite}, {props.data.city} - {props.data.zipcode} <br/>
                            <strong>Telefone:</strong> {props.data.phone} <br/>
                            <strong>Site:</strong> {props.data.website} <br/>
                            <strong>Empresa:</strong> {props.data.companyName} <br/>
                            <strong>Frase de Efeito:</strong> {props.data.catchPhrase} <br/>
                            <strong>BS:</strong> {props.data.bs} <br/>
                        </p>
                        <Link className="btn btn-primary" to={`/editar-usuario/${props.data.id}`} alt="Editar">
                            Editar
                        </Link> &ensp;
                        <Link className="btn btn-warning" to={`/usuarios/${props.data.id}/posts`} alt="Posts">
                            Visualizar posts deste usuário
                        </Link> &ensp;
                        <button onClick={props.onClose} className="btn btn-secondary">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    );
}
