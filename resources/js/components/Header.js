import React from 'react'
import { Link } from 'react-router-dom'
import './Header.css'

const Header = () => (
    <nav className='navbar navbar-expand-md navbar-light navbar-laravel'>
        <div className='container'>
            <Link className='navbar-brand' to='/'>Usuários</Link>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link className='nav-link' to='/'>Inicio</Link>
                    </li>
                    <li className="nav-item">
                        <Link className='nav-link' to='/importar'>Importar JSON</Link>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
)

export default Header
