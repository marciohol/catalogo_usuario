import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Header'

import { render } from 'react-dom'
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'

import UsersList from './pages/UsersList'
import NewUser from './pages/NewUser'
import EditUser from './pages/EditUser'

import PostsList from './pages/PostsList'

import Imports from './pages/Imports'

export default function App() {
    const options = {
        // you can also just use 'bottom center'
        position: positions.BOTTOM_CENTER,
        timeout: 5000,
        offset: '30px',
        // you can also just use 'scale'
        transition: transitions.SCALE
    }
    return (
        <AlertProvider template={AlertTemplate} {...options}>
            <BrowserRouter>
                <div>
                    <Header />
                    <Switch>
                        <Route exact path='/' component={UsersList} />
                        <Route exact path='/novo-usuario' component={NewUser} />
                        <Route path='/importar' component={Imports} />
                        <Route path='/editar-usuario/:id' component={EditUser} />
                        <Route path='/usuarios/:id/posts' component={PostsList} />
                    </Switch>
                </div>
            </BrowserRouter>
        </AlertProvider>
    )
}

ReactDOM.render(<App />, document.getElementById('app'))
