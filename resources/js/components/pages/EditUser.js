import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom'
import api from '../services/api';
import { useAlert } from 'react-alert';

export default function NewUser(props) {
    const { id } = props.match.params;

    const [name, setName] = useState('');
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [street, setStreet] = useState('');
    const [suite, setSuite] = useState('');
    const [city, setCity] = useState('');
    const [zipcode, setZipcode] = useState('');
    const [lat, setLat] = useState('');
    const [lng, setLng] = useState('');
    const [phone, setPhone] = useState('');
    const [website, setWebsite] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [catchPhrase, setCatchPhrase] = useState('');
    const [bs, setBs] = useState('');
    const [loading, setLoading] = useState(false);
    const alert = useAlert();

    async function loadUser() {
        const { data } = await api.get(`/api/users/${id}`);

        setName(data.data.name);
        setUsername(data.data.username);
        setEmail(data.data.email);
        setStreet(data.data.street);
        setSuite(data.data.suite);
        setCity(data.data.city);
        setZipcode(data.data.zipcode);
        setLat(data.data.lat);
        setLng(data.data.lng);
        setPhone(data.data.phone);
        setWebsite(data.data.website);
        setCompanyName(data.data.companyName);
        setCatchPhrase(data.data.catchPhrase);
        setBs(data.data.bs);

    }

    useEffect(() => {
        loadUser();
    }, []);

    async function handleSubmit(e) {
        e.preventDefault();
        setLoading(true);

        let object = {
            name,
            username,
            email,
            street,
            suite,
            city,
            zipcode,
            lat,
            lng,
            phone,
            website,
            companyName,
            catchPhrase,
            bs
        };

        const { data } = await api.put(`/api/users/${id}`, object);

        if(!data.success) {
            setLoading(false);
            alert.error(data.message);
        }

        if(data.success) {
            alert.success(data.message);

            props.history.push('/');
        }
    }

    return (

        <div className='container py-4'>
            <div className='row justify-content-center'>
                <div className='col-md-12'>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item">
                                <Link to={`/`} alt="Início">
                                    Início
                                </Link>
                            </li>
                            <li className="breadcrumb-item active" aria-current="page">Editar Usuário</li>
                        </ol>
                    </nav>
                    <div className='card'>
                        <div className='card-header'>Editar Usuário</div>
                        <div className='card-body'>
                            <form onSubmit={handleSubmit}>
                                <div className="form-row">
                                    <div className="form-group col-md-12">
                                        <label htmlFor="name">Nome Completo</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="name"
                                            value={name}
                                            onChange={e => setName(e.target.value)}
                                            className="form-control"
                                            id="name"
                                            placeholder="Nome Completo"
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                        <label htmlFor="username">Nome de Usuário</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="username"
                                            value={username}
                                            onChange={e => setUsername(e.target.value)}
                                            className="form-control"
                                            id="username"
                                            placeholder="Username"
                                            required
                                        />
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label htmlFor="email">Email</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="email"
                                            value={email}
                                            onChange={e => setEmail(e.target.value)}
                                            className="form-control"
                                            id="email"
                                            placeholder="Email"
                                            required
                                        />
                                    </div>
                                    <div className="form-group col-md-8">
                                        <label htmlFor="street">Rua</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="street"
                                            value={street}
                                            onChange={e => setStreet(e.target.value)}
                                            className="form-control"
                                            id="street"
                                            placeholder="Rua"
                                        />
                                    </div>
                                    <div className="form-group col-md-4">
                                        <label htmlFor="suite">Suite</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="suite"
                                            value={suite}
                                            onChange={e => setSuite(e.target.value)}
                                            className="form-control"
                                            id="suite"
                                            placeholder="Suite"
                                        />
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label htmlFor="city">Cidade</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="city"
                                            value={city}
                                            onChange={e => setCity(e.target.value)}
                                            className="form-control"
                                            id="city"
                                            placeholder="Cidade"
                                        />
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label htmlFor="zipcode">CEP</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="zipcode"
                                            value={zipcode}
                                            onChange={e => setZipcode(e.target.value)}
                                            className="form-control"
                                            id="zipcode"
                                            placeholder="CEP"
                                        />
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label htmlFor="lat">Latitude</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="lat"
                                            value={lat}
                                            onChange={e => setLat(e.target.value)}
                                            className="form-control"
                                            id="lat"
                                            placeholder="Latitude"
                                        />
                                    </div>
                                    <div className="form-group col-md-3">
                                        <label htmlFor="lng">Longitude</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="lng"
                                            value={lng}
                                            onChange={e => setLng(e.target.value)}
                                            className="form-control"
                                            id="lng"
                                            placeholder="Longitude"
                                        />
                                    </div>
                                    <div className="form-group col-md-4">
                                        <label htmlFor="phone">Telefone</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="phone"
                                            value={phone}
                                            onChange={e => setPhone(e.target.value)}
                                            className="form-control"
                                            id="phone"
                                            placeholder="Telefone"
                                        />
                                    </div>
                                    <div className="form-group col-md-4">
                                        <label htmlFor="website">Site</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="website"
                                            value={website}
                                            onChange={e => setWebsite(e.target.value)}
                                            className="form-control"
                                            id="website"
                                            placeholder="Site"
                                        />
                                    </div>
                                    <div className="form-group col-md-4">
                                        <label htmlFor="companyName">Nome da Empresa</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="companyName"
                                            value={companyName}
                                            onChange={e => setCompanyName(e.target.value)}
                                            className="form-control"
                                            id="companyName"
                                            placeholder="Nome da Empresa"
                                        />
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label htmlFor="catchPhrase">Frase de Efeito</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="catchPhrase"
                                            value={catchPhrase}
                                            onChange={e => setCatchPhrase(e.target.value)}
                                            className="form-control"
                                            id="catchPhrase"
                                            placeholder="Frase de Efeito"
                                        />
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label htmlFor="bs">BS</label>
                                        <input
                                            type="text"
                                            autoComplete="off"
                                            name="bs"
                                            value={bs}
                                            onChange={e => setBs(e.target.value)}
                                            className="form-control"
                                            id="bs"
                                            placeholder="BS"
                                        />
                                    </div>
                                </div>
                                <button type="submit" disabled={loading} className={`btn btn-primary`}>{loading ? <i className="fas fa-spinner fa-pulse"></i> : 'Atualizar Usuário'}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
