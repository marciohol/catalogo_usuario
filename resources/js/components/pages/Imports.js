import React, { useState } from 'react';
import axios from 'axios';

import api from '../services/api';
import { useAlert } from 'react-alert';

export default function Imports(props) {
    const [selectedOption, setSelectedOption] = useState('url');
    const [url, setUrl] = useState('');
    const [json, setJson] = useState('');
    const [table, setTable] = useState('users');
    const [loading, setLoading] = useState(false);
    const alert = useAlert();

    function handleUrlChange(e) {
        setUrl(e.target.value);
    }

    function handleJsonChange(e) {
        setJson(e.target.value);
    }

    async function handleSubmit(e) {
        e.preventDefault();

        setLoading(true);

        if(selectedOption == 'url') {
            var { data } = await axios.get(url);
        }

        if(selectedOption == 'json') {
            var data = JSON.parse(json);
        }

        let object = {
            table,
            data,
        }

        const response = await api.post('api/imports', object);

        if(!response.data.success) {
            setLoading(false);
            alert.error(response.data.message);
        }

        if(response.data.success) {
            alert.success(response.data.message);

            props.history.push('/');
        }
    }

    return (
        <div className='container py-4'>
            <div className='row justify-content-center'>
                <div className='col-md-12'>
                    <nav aria-label="breadcrumb">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item active" aria-current="page">Importar JSON</li>
                            </ol>
                        </nav>
                    <div className='card'>
                        <div className='card-header'>Importar Usuários</div>
                        <div className='card-body'>
                            <form onSubmit={handleSubmit}>
                                <div className="form-group">
                                    <div className="form-check form-check-inline">
                                        <input
                                            className="form-check-input"
                                            type="radio"
                                            name="typeImport"
                                            id="url"
                                            value="url"
                                            checked={selectedOption === 'url'}
                                            onChange={e => setSelectedOption(e.target.value)}
                                        />
                                        <label className="form-check-label" htmlFor="url">URL</label>
                                    </div>
                                    <div className="form-check form-check-inline">
                                        <input
                                            className="form-check-input"
                                            type="radio"
                                            name="typeImport"
                                            id="json"
                                            value="json"
                                            checked={selectedOption === 'json'}
                                            onChange={e => setSelectedOption(e.target.value)}
                                            placeholder="Cole aqui o JSON"
                                        />
                                        <label className="form-check-label" htmlFor="json">JSON</label>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="table">Escolha a tabela que irá inserir os dados</label>
                                    <select id="table" name="table" onChange={e => setTable(e.target.value)} className="form-control">
                                        <option value="users">Usuários</option>
                                        <option value="posts">Posts</option>
                                    </select>
                                </div>
                                {
                                    selectedOption === 'url' ? (
                                        <div className="form-group">
                                            <label htmlFor="url">URL</label>
                                            <input
                                                type="text"
                                                name="url"
                                                value={url}
                                                onChange={handleUrlChange}
                                                className="form-control"
                                                id="url"
                                                placeholder="http://teste/teste.json"
                                                required
                                            />
                                        </div>
                                    ) : (
                                        <div className="form-group">
                                            <label htmlFor="url">JSON</label>
                                            <textarea
                                                rows="5"
                                                name="json"
                                                value={json}
                                                onChange={handleJsonChange}
                                                className="form-control"
                                                id="url"
                                                required
                                            />
                                        </div>
                                    )
                                }
                                <button type="submit" disabled={loading} className={`btn btn-primary`}>{loading ? <i className="fas fa-spinner fa-pulse"></i> : 'Importar Dados'}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
