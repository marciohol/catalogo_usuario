import React, { useState, useEffect, Fragment } from 'react'
import { Link } from 'react-router-dom'
import api from '../services/api';
import { useAlert } from 'react-alert';

import Pagination from '../Pagination';
import Modal from '../Modal';

import './PostsList.css';

export default function PostsList(props) {
    const { id } = props.match.params;

    const [user, setUser] = useState({});
    const [posts, setPosts] = useState([]);
    const [pageOfItems, setPageOfItems] = useState([]);
    const [loading, setLoading] = useState(false);

    async function loadPosts() {
        setLoading(true)

        const { data } = await api.get(`/api/users/${id}/posts`);

        setUser(data.data);
        setPosts(data.data.posts)

        setLoading(false)
    }

    function onChangePage(pageItems) {
        setPageOfItems(pageItems)
    }

    useEffect(() => {
        loadPosts();
    }, []);

    return (
        <Fragment>
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-12'>
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                    <Link to={`/`} alt="Início">
                                        Início
                                    </Link>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">Posts</li>
                            </ol>
                        </nav>
                        <div className='card'>
                            <div className='card-header'>Posts de {user.name}</div>
                            <div className='card-body'>
                                <br />
                                <table className="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">Título</th>
                                            <th scope="col">Mensagem</th>
                                            {/* <th scope="col">Ações</th> */}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            loading ? (
                                                <tr>
                                                    <td colSpan="3" align="center" style={{ fontSize: 25 }}> <i className="fas fa-spinner fa-pulse"></i> </td>
                                                </tr>
                                            ) : (
                                                pageOfItems.map(post => (
                                                    <tr key={post.id}>
                                                        <td>{post.title}</td>
                                                        <td>{post.body}</td>
                                                        {/* <td className="td-actions">
                                                            <button onClick={() => toggleModal(post)} className="btn btn-primary btn-sm" alt="Visualizar Post"><i className="fas fa-search"></i></button>
                                                        </td> */}
                                                    </tr>
                                                ))
                                            )
                                        }
                                    </tbody>
                                </table>

                                <Pagination items={posts} onChangePage={onChangePage} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
