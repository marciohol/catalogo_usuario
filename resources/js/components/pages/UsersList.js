import React, { useState, useEffect, Fragment } from 'react'
import { Link } from 'react-router-dom'
import api from '../services/api';
import { useAlert } from 'react-alert';

import Pagination from '../Pagination';
import Modal from '../Modal';

import './UsersList.css';

export default function UsersList() {
    const [users, setUsers] = useState([]);
    const [pageOfItems, setPageOfItems] = useState([]);
    const [isModalOpen, setModalIsOpen] = useState(false);
    const [userModal, setUserModal] = useState({});
    const [loading, setLoading] = useState(false);

    const alert = useAlert();

    async function loadUsers() {
        setLoading(true)

        const { data } = await api.get('api/users')

        setUsers(data.data)

        setLoading(false)
    }

    function onChangePage(pageItems) {
        setPageOfItems(pageItems)
    }

    async function handleButtonDelete(id) {
        const { data } = await api.delete(`/api/users/${id}`);

        loadUsers();

        if(!data.success) {
            alert.error(data.message);
        }

        if(data.success) {
            alert.success(data.message);
        }
    }

    function toggleModal(user) {
        let modalOpen = !isModalOpen;
        setUserModal(user);
        setModalIsOpen(modalOpen);
    }

    useEffect(() => {
        loadUsers();
    }, []);

    return (
        <Fragment>
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-12'>
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item active" aria-current="page">Início</li>
                            </ol>
                        </nav>
                        <div className='card'>
                            <div className='card-header'>Todas os usuários</div>
                            <div className='card-body'>
                                <Link className='btn btn-primary btn-sm mb-3' to='/novo-usuario'>
                                    Adicionar novo usuário
                                </Link>
                                <br />
                                <table className="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">Nome</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            loading ? (
                                                <tr>
                                                    <td colSpan="3" align="center" style={{ fontSize: 25 }}> <i className="fas fa-spinner fa-pulse"></i> </td>
                                                </tr>
                                            ) : (
                                                pageOfItems.map(user => (
                                                    <tr key={user.id}>
                                                        <td>{user.name}</td>
                                                        <td>{user.email}</td>
                                                        <td className="td-actions">
                                                            <button onClick={() => toggleModal(user)} className="btn btn-primary btn-sm" alt="Visualizar"><i className="fas fa-search"></i></button>
                                                            <Link className="btn btn-primary btn-sm" to={`/editar-usuario/${user.id}`} alt="Editar">
                                                                <i className="far fa-edit"></i>
                                                            </Link>
                                                            <button alt="Excluir" onClick={() => { if (window.confirm('Você deseja deletar esse usuario?')) handleButtonDelete(user.id) }} className="btn btn-primary btn-sm" href="#" role="button"><i className="far fa-trash-alt"></i></button>
                                                            <Link className="btn btn-primary btn-sm" to={`/usuarios/${user.id}/posts`} alt="Listar posts">
                                                                <i className="fas fa-list"></i>
                                                            </Link>
                                                        </td>
                                                    </tr>
                                                ))
                                            )
                                        }
                                    </tbody>
                                </table>

                                <Pagination items={users} onChangePage={onChangePage} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Modal
                show={isModalOpen}
                onClose={toggleModal}
                data={userModal}
            />
        </Fragment>
    )
}
