# Catalogo de Usuários

> Marcio Holanda

## Criando servidor docker

```sh
docker-compose up -d
```

## Instalação

```sh
docker-compose exec catalogo composer install
docker-compose exec catalogo yarn install
docker-compose exec catalogo yarn dev
```

Renomeie o arquivo .env.example para .env<br />
Gere a chave do projeto com o comando

```sh
docker-compose exec catalogo php artisan key:generate
```

Faça a migração das tabelas do banco com <br />
o comando

```sh
docker-compose exec catalogo php artisan migrate
```

Acesse o Servidor

```sh
http://localhost
```
